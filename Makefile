EER_MD = $(wildcard eer-*/index.md)
EER_PIC = $(wildcard eer-*/*.pic)

OUT ?= public
EER_HTML := $(addprefix $(OUT)/, $(addsuffix .html,$(basename $(EER_MD))))
EER_SVG := $(addprefix $(OUT)/, $(addsuffix .svg,$(basename $(EER_PIC))))

HEADER := template/header.xml
FOOTER := template/footer.xml

all: $(EER_HTML) $(EER_SVG) $(OUT)/index.html $(OUT)/eer-005/eris-cache.ttl

$(OUT)/index.html: index.md $(HEADER) $(FOOTER)
	mkdir -p $(OUT)
	(cat $(HEADER); lowdown $< ; cat $(FOOTER)) >$@

$(OUT)/%/index.html: %/index.md $(HEADER) $(FOOTER)
	mkdir -p $(dir $@)
	(cat $(HEADER); lowdown $< ; cat $(FOOTER)) >$@

# Should have just used tup...
.PHONY: $(EER_SVG)
$(EER_SVG):
	pikchr --svg-only $(addsuffix .pic,$(basename $(subst $(OUT),.,$@))) > $@

$(OUT)/eer-005/eris-cache.ttl: eer-005/eris-cache.ttl
	mkdir -p $(dir $@)
	cp $< $@
