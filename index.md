# ERIS Extensions and Recommendations

This is a collection of extensions, recommendations and best-practices related to the [ERIS encoding](http://purl.org/eris).

The extensions and recommendations provide inspiration and guidelines for using ERIS practically. They are subject to on-going research and development.

The format of these documents is less formal and stable than the specification of the encoding.

Contributions and discussions are very welcome! Get in touch via [the mailing list or IRC](https://eris.codeberg.page/#Contact).

For more information on ERIS see the [project page](https://eris.codeberg.page/).

## Content

- [ERIS over HTTP](./eer-001/): Convention on how to resolve blocks of ERIS encoded content over HTTP(S) using [RFC2169](https://datatracker.ietf.org/doc/html/rfc2169).
- [ERIS and RDF](./eer-002/): How ERIS can be used for content-addressable RDF.
- [ERIS Canons](./eer-003): Log data-structure with an effiencient append operation.
- [ERIS and Magnet URI](./eer-004/): How to reference ERIS encoded content with Magnet URIs. This allows further informations such as filename and lenght of content to be encoded in a URI.
- [ERIS Cache](./eer-005/): A RDF vocabulary for describing content cached with ERIS.
- [ERIS over IPFS](#): TODO
- [ERIS and Immutable file-systems](#): TODO
- [ERIS and file archive formats](#): TODO

## Repository

These documents are generated from a [git repository](https://codeberg.org/eris/eer).

## License

[CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/)
