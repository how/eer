<div class="abstract" id="org744b296">
<p>
An extension to the W3C PROVenance Interchange Ontology to describe the activity of content-addressed caching with the ERIS encoding.
</p>

</div>


# Table of Contents

1.  [Introduction](#orgd81cc93)
2.  [Vocabulary](#orgc51c88f)


<a id="orgd81cc93"></a>

# Introduction

By content-addressing RDF data we can increase the robustness of systems as the content is decoupled from a single location where it is hosted [cite:ContentAddressableRDF2020].

A lot of content is already published using location-addressed identifiers (URLs). Such content can be made content-addressable. This improves the availability of the location-addressed content and may be seen as a content-addressed cache.

We propose a vocabulary to describe the original source of the content-addressed cache as well as other circumstances that describe the creation of the cache as an extension to the W3C PROV-O ontology [cite:lebo2013prov].

The proposed ontology is tied to the ERIS encoding scheme [cite:ERIS<sub>2020</sub>].


<a id="orgc51c88f"></a>

# Vocabulary

    @prefix ec: <http://purl.org/eris/cache#> .
    @prefix prov: <http://www.w3.org/ns/prov#> .
    @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
    @prefix owl: <http://www.w3.org/2002/07/owl#> .
    
    <http://purl.org/eris/cache#>
        a owl:Ontology ;
        rdfs:label "ERIS Cache Vocabulary"@en ;
        rdfs:comment "An extension to the W3C PROVenance Interchange Ontology to describe the Activity of content-addressed caching with the ERIS encoding."@en .
    
    ec:CreateCache
        a rdfs:Class ;
        rdfs:label "CreateCache" ;
        rdfs:comment "The activity of creating an ERIS encoded, content-addressed cache of some content.";
        rdfs:subClassOf prov:Activity .

bibliography:refs.bib

<p>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">ERIS Cache</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://openengiadina.net/" property="cc:attributionName" rel="cc:attributionURL">openEngiadina</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</p>

