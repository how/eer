(use-modules
 (guix packages)
 (guix build-system gnu)
 (gnu packages markup)
 (gnu packages pikchr))

(package
 (name "eris-site")
 (version "0.0.0")
 (source #f)
 (build-system gnu-build-system)
 (native-inputs
  (list lowdown pikchr))
 (synopsis #f)
 (description #f)
 (home-page #f)
 (license #f))
