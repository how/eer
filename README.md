# ERIS Extensions and Recommendations

This repository contains documentation of extensions, recommendations and best-practices related to [ERIS](https://purl.org/eris).

## Writing and Publishing

EERs are located in the `eer-*` folders in the `index.md` files.

EERs and a index page can be rendered to HTML with [lowdown](https://kristaps.bsd.lv/lowdown/) by running `make`.

Everything is published using [Codeberg Pages](https://codeberg.page/), i.e. the `pages` branch of this repository is made available at [https://eris.codeberg.page/eer](https://eris.codeberg.page/eer).

You can checkout the `pages` branch in an other git worktree: `git worktree add ../eer-pages pages` and then render to the pages worktree with `OUT=../eer-pages make`. To publish you will need to commit and push changes in the `pages` branch.

## License

[CC-BY-SA-4.0](./COPYING)
